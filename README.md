# EzCore-Bukkit

EzCore-Bukkit é uma framework simples para **Bukkit API** com alguns recursos úteis (ou nem tanto).
**Atenção: ezCore não é uma super-produção. Lembre-se que ela está em testes e o código pode ter falhas. Sugestões são aceitas :D**

## Dependências
[EzCore](http://bitbucket.org/ezcodeteam/EzCore)

[Spigot API](https://hub.spigotmc.org/nexus/content/repositories/snapshots/)

[CraftBukkit](https://www.spigotmc.org/wiki/buildtools/)

## Maven
```xml
<dependency>
  <groupId>ez.code.bukkit</groupId>
  <artifactId>EzCore-Bukkit</artifactId>
  <version>LATEST</version>
</dependency>

<repository>
  <id>EzCore-Bukkit</id>
  <url>https://bitbucket.org/ezCodeTeam/ezCore-Bukkit/raw/master/maven-repo/repository/</url>
</repository>
```
