package ez.code.bukkit.core;

/**
 * 
 * @author Paulo
 *
 */
public class EzCoreBukkit {

	/*
	 * TODO JavaDoc TODO Updater FIXME Salvar a versão apenas em um lugar.
	 */

	private static String version = "0.0.5";

	public static String getVersion() {
		return version;
	}

	public static boolean isIndev() {
		return getVersion().endsWith("indev");
	}
}
