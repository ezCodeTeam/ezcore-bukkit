package ez.code.bukkit.core.bukkit;

import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.plugin.java.JavaPlugin;

import ez.code.bukkit.core.EzCoreBukkit;
import ez.code.core.utils.EzHTML;

public class EzBukkit extends JavaPlugin {

	@Override
	public void onEnable() {
		getLogger().log(Level.INFO, "EzCore-Bukkit " + EzCoreBukkit.getVersion() + " iniciado.");
		if (EzCoreBukkit.isIndev())
			getLogger().log(Level.INFO, "Você está usando uma versão de Testes!");
		getLogger().log(Level.INFO,
				"Saiba mais sobre o EzCore-Bukkit. Acesse https://www.bitbucket.org/ezcodeteam/ezcore-bukkit");
		try {
			String data = EzHTML.getData("https://bitbucket.org/ezCodeTeam/ezCore-Bukkit/raw/master/pom.xml");
			data = data.substring(data.indexOf("<version>") + 9, data.indexOf("</version>"));
			if (!data.equals(EzCoreBukkit.getVersion())) {
				getLogger().log(Level.INFO,
						"Seu EzCore-Bukkit está desatualizado. Baixe a nova versão em https://www.bitbucket.org/ezcodeteam/ezcore-bukkit");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
