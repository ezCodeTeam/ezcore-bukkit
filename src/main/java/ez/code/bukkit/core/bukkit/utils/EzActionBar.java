package ez.code.bukkit.core.bukkit.utils;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class EzActionBar {

	public static void sendActionMessage(Player p, String message, int fadeIn, int stay, int fadeOut) {
		PacketPlayOutChat packet = new PacketPlayOutChat(
				ChatSerializer.a("{\"text\" : \"" + EzChat.colorMessage(message) + "\"}"), (byte) 2);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}

}
