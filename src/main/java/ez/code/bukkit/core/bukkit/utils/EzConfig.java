package ez.code.bukkit.core.bukkit.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class EzConfig {

	private String name;
	private YamlConfiguration config;
	private Plugin main;
	private File file;

	public EzConfig(String name, Plugin main) {
		setName(name);
		setMain(main);
		reloadConfig();
	}

	public EzConfig(String name, Plugin main, File file) {
		setName(name);
		setMain(main);
		setFile(file);
		reloadConfig();
	}

	public void reloadConfig() {
		if (file == null)
			file = new File(main.getDataFolder(), name);
		setConfig(YamlConfiguration.loadConfiguration(getFile()));
	}

	public void saveDefaultConfig(boolean overide) {
		main.saveResource(getName(), overide);
	}

	public void saveConfig() {
		try {
			config.save(getFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// MÉTODOS DE ACESSO
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public YamlConfiguration getConfig() {
		return config;
	}

	private void setConfig(YamlConfiguration config) {
		this.config = config;
	}

	public Plugin getMain() {
		return main;
	}

	public void setMain(Plugin main) {
		this.main = main;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}
