package ez.code.bukkit.core.bukkit.utils;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;

public class EzTitle {

	public static void sendTitle(Player p, String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		PacketPlayOutTitle packetSubTitle = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE,
				ChatSerializer.a("{\"text\": \"" + EzChat.colorMessage(subtitle) + "\"}"), fadeIn, stay, fadeOut);
		PacketPlayOutTitle packetTitle = new PacketPlayOutTitle(EnumTitleAction.TITLE,
				ChatSerializer.a("{\"text\": \"" + EzChat.colorMessage(title) + "\"}"), fadeIn, stay, fadeOut);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packetSubTitle);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packetTitle);
	}

}
