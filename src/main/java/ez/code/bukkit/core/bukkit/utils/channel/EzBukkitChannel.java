package ez.code.bukkit.core.bukkit.utils.channel;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class EzBukkitChannel implements PluginMessageListener {

	private Plugin main;
	private ArrayList<EzBukkitChannelListener> listeners = new ArrayList<>();
	private String channel;

	public EzBukkitChannel(Plugin main, String channel) {
		this.main = main;
		this.channel = channel;
		getMain().getServer().getMessenger().registerOutgoingPluginChannel(getMain(), channel);
		getMain().getServer().getMessenger().registerIncomingPluginChannel(getMain(), channel, this);
	}

	public void addListener(EzBukkitChannelListener listener) {
		listeners.add(listener);
	}

	public void removeListener(EzBukkitChannelListener listener) {
		listeners.remove(listener);
	}

	public ArrayList<EzBukkitChannelListener> getListeners() {
		return listeners;
	}

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		ByteArrayDataInput in = ByteStreams.newDataInput(message);
		for (EzBukkitChannelListener listener : listeners) {
			System.out.println("OLHA A MENSAGEM!");
			listener.messageReceived(player, message, in);
		}
	}

	private Plugin getMain() {
		return main;
	}

	public void sendMessage(Player player, String message) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(message);
		player.sendPluginMessage(getMain(), channel, out.toByteArray());
	}

	public void sendMessage(Player player, String[] message) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		for (String str : message) {
			out.writeUTF(str);
		}
		player.sendPluginMessage(getMain(), channel, out.toByteArray());
	}

	public void sendMessage(String message) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(message);
		Bukkit.getServer().sendPluginMessage(getMain(), channel, out.toByteArray());
	}

	public void sendMessage(String[] message) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		for (String str : message) {
			out.writeUTF(str);
		}
		Bukkit.getServer().sendPluginMessage(getMain(), channel, out.toByteArray());
	}
}
