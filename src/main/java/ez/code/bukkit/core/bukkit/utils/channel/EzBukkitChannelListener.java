package ez.code.bukkit.core.bukkit.utils.channel;

import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataInput;

public interface EzBukkitChannelListener {
	public void messageReceived(Player player, byte[] message, ByteArrayDataInput in);
}
