package ez.code.bukkit.core.bungeecord.channel;

import java.util.ArrayList;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

public class EzBungeeChannel implements Listener {

	private Plugin main;
	private String channel;
	private ArrayList<EzBungeeChannelListener> listeners = new ArrayList<EzBungeeChannelListener>();

	public EzBungeeChannel(Plugin main, String channel) {
		this.main = main;
		this.channel = channel;
		main.getProxy().registerChannel(channel);
		main.getProxy().getPluginManager().registerListener(main, this);
	}

	public void addListener(EzBungeeChannelListener listener) {
		listeners.add(listener);
	}

	public void removeListener(EzBungeeChannelListener listener) {
		listeners.remove(listener);
	}

	public void sendMessage(ProxiedPlayer player, String message) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(message);
		// player.sendPluginMessage(getMain(), channel, out.toByteArray());
		player.getServer().sendData(channel, out.toByteArray());

	}

	public void sendMessage(ProxiedPlayer player, String[] message) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		for (String str : message) {
			out.writeUTF(str);
		}
		// player.sendPluginMessage(getMain(), channel, out.toByteArray());
		player.getServer().sendData(channel, out.toByteArray());
	}

	public void sendMessage(String message, String serverName) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(message);
		// Bukkit.getServer().sendPluginMessage(getMain(), channel, out.toByteArray());
		main.getProxy().getServers().get(serverName).sendData(channel, out.toByteArray());
	}

	public void sendMessage(String[] message, String serverName) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		for (String str : message) {
			out.writeUTF(str);
		}
		// Bukkit.getServer().sendPluginMessage(getMain(), channel, out.toByteArray());
		main.getProxy().getServers().get(serverName).sendData(channel, out.toByteArray());
	}

	@EventHandler
	public void messageEvent(PluginMessageEvent e) { // TODO static, um para todos os canais.
		if (e.getTag().equals(channel)) {
			for (EzBungeeChannelListener listener : listeners) {
				listener.messageReceived(e.getData(), e.getReceiver(), e.getSender());
			}
		}
	}

}
