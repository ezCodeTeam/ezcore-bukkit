package ez.code.bukkit.core.bungeecord.channel;

import net.md_5.bungee.api.connection.Connection;

public interface EzBungeeChannelListener {
	public void messageReceived(byte[] data, Connection receiver, Connection sender);
}
